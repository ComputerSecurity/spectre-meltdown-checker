#! /bin/sh
# Spectre variant 2 (CVE-2017-5715) CPU microcode checker
# CERN Computer Security Team
VERSION=0.4

# print status function
pstatus() {
	  case "$1" in
			  red)    col="\033[101m\033[30m";;
			  green)  col="\033[102m\033[30m";;
			  yellow) col="\033[103m\033[30m";;
			  blue)   col="\033[104m\033[30m";;
			  *)      col="";;
	  esac
	  /bin/echo -ne "$col $2 \033[0m"
	  [ -n "$3" ] && /bin/echo -n " ($3)"
	  /bin/echo
}

# Try to check loaded microcode (only works if root)
check_microcode() {
	/bin/echo -n "> Microcode loaded: "
	version=$(cat /sys/devices/system/cpu/cpu0/microcode/version 2>/dev/null)
	if [ $? -ne 0 ]; then
		pstatus yellow "UNKNOWN" "Unable to read /sys/devices/system/cpu/cpu0/microcode/version. This test needs to be run as root"
		return
	fi
	case $1 in
		06-3c-03) expected_version=0x0023;;
		06-3d-04) expected_version=0x0028;;
		06-3e-04) expected_version=0x042a;;
		06-3f-02) expected_version=0x003b;;
		06-3f-04) expected_version=0x0010;;
		06-45-01) expected_version=0x0021;;
		06-46-01) expected_version=0x0018;;
		06-47-01) expected_version=0x001b;;
		06-4e-03) expected_version=0x00c2;;
		06-4f-01) expected_version=0xb000025;;
		06-55-04) expected_version=0x200003c;;
		06-56-02) expected_version=0x0014;;
		06-56-03) expected_version=0x7000011;;
		06-5e-03) expected_version=0x00c2;;
		06-7a-01) expected_version=0x0022;;
		06-8e-09|06-8e-0a|06-9e-09|06-9e-0a|06-9e-0b) expected_version=0x0080;;
	esac
	if [ "$(printf "%d" "$version")" -ge  "$(printf "%d" "$expected_version")" ]; then
		pstatus yellow "PATCHED/UNSTABLE" "Potentially unstable microcode from Intel/Redhat loaded, system protected against spectre v2"
	else
		pstatus red "OUTDATED/STABLE" "Old microcode detected, no protection against spectre v2"
	fi
}

if [ "$(uname -s)" != "Linux" ]; then
	/bin/echo -e "\033[1;31mThis tool supports Linux based Operating Systems only\033[0m"
	exit 10
fi

if [ -x /usr/sbin/virt-what -a -n "$(/usr/sbin/virt-what 2>/dev/null)"  -o \
     -x /usr/bin/systemd-detect-virt -a "$(/usr/bin/systemd-detect-virt 2>/dev/null)" != "none" -o \
     -x /opt/puppetlabs/bin/facter -a "$(/opt/puppetlabs/bin/facter is_virtual 2>/dev/null)" = "true" ]; then
	/bin/echo -e "\033[1;31mThis check tool should only be ran on physical hardware.\033[0m"
	exit 11
fi

/bin/echo -e "\033[1;34mSpectre variant 2 (CVE-2017-5715) CPU microcode checker v$VERSION\033[0m"
/bin/echo

cpu_family=$(printf '%02x' $(cat /proc/cpuinfo | grep "^cpu family" | cut -d ':' -f2 | head -1 | tr -d '[:space:]'))
cpu_model=$(printf '%02x' $(cat /proc/cpuinfo | grep "^model" | cut -d ':' -f2 | head -1 | tr -d '[:space:]'))
stepping=$(printf '%02x' $(cat /proc/cpuinfo | grep "^stepping" | cut -d ':' -f2 | head -1 | tr -d '[:space:]'))

cpu_details="${cpu_family}-${cpu_model}-${stepping}"
ret_code=1

/bin/echo -n "> CPU microcode: "

if grep -q AMD /proc/cpuinfo; then
	if [ "$cpu_family" = "17" ]; then
		pstatus green "AVAILABLE" "CPU microcode available in latest linux-firmware package";
		ret_code=0
	else
		pstatus red "NOT AVAILABLE" "CPU microcode not available";
	fi
elif grep -q Intel /proc/cpuinfo; then
	case "$cpu_details" in
		06-3f-02|06-4f-01|06-55-04) pstatus yellow "AVAILABLE/REMOVED" "CPU microcode released by Intel available in microcode_ctl package (but not the latest)"; check_microcode "$cpu_details"; ret_code=0;;
		06-4f-01) pstatus yelow "AVAILABLE/REMOVED" "CPU microcode, not released by Intel, available in microcode_ctl package (but not the latest)"; check_microcode "$cpu_details"; ret_code=0;;
		06-3c-03|06-3d-04|06-3e-04|06-3f-04|06-45-01|06-46-01|06-47-01|06-4e-03|06-56-02|06-56-03|06-5e-03|06-7a-01|06-8e-09|06-8e-0a|06-9e-09|06-9e-0a|06-9e-0b) pstatus yellow "PENDING" "Intel has recently released a microcode update, which has not yet been repackaged by RedHat"; check_microcode "$cpu_details"; ret_code=0;;
		*) pstatus red "NOT AVAILABLE" "CPU microcode not available";;
	esac
fi

/bin/echo
exit $ret_code
